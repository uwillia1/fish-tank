using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FishTank.Model;
using FishTank.Model.FishModel;

namespace FishTank.View.Forms
{
    /// <summary>
    ///     Manages the form that displays the fish tank.
    /// </summary>
    public partial class FishTank
    {
        #region Data members

        private readonly FishTankManager fishTankManager;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTank" /> class.
        ///     Precondition: None
        /// </summary>
        public FishTank()
        {
            this.InitializeComponent();

            this.fishTankManager = new FishTankManager(this.fishTankPictureBox.Height, this.fishTankPictureBox.Width);
            this.fishTankManager.PlaceFishInTank(0);
            this.animationTimer.Start();
        }

        #endregion

        #region Event generated methods

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            Refresh();
        }

        private void fishTankCanvasPictureBox_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            this.fishTankManager.Update(g);
        }

        #endregion

        private void FishTank_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void buttonFillTank_Click(object sender, EventArgs e)
        {
            var userInput = this.textBoxUserInput.Text;
            int numberOfFish;
            var result = int.TryParse(userInput, out numberOfFish);
            if (result)
            {
                this.fishTankManager.PlaceFishInTank(numberOfFish);
                this.labelNumber.Text = Convert.ToString(numberOfFish);
            }
            this.updateDataGridView(this.fishTankManager.SchoolOfFish);
        }

        private void updateDataGridView(IEnumerable<Fish> schoolOfFish)
        {
            this.dataGridView1.Rows.Clear();
            foreach (var fish in schoolOfFish)
            {
                var weightString = fish.Weight.ToString("F3");
                var typeString = fish.GetType().Name;
                this.dataGridView1.Rows.Add(weightString, typeString);
            }
        }

        private void buttonEmptyTank_Click(object sender, EventArgs e)
        {
            this.fishTankManager.PlaceFishInTank(0);
            this.labelNumber.Text = Convert.ToString(0);
            this.dataGridView1.Rows.Clear();
        }

        private void buttonSortByWeight_Click(object sender, EventArgs e)
        {
            this.updateDataGridView(this.fishTankManager.SortByWeight());
        }

        private void buttonSortByWeightAndType_Click(object sender, EventArgs e)
        {
            this.updateDataGridView(this.fishTankManager.SortByTypeThenWeight());
        }
    }
}