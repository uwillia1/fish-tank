using System.ComponentModel;
using System.Windows.Forms;

namespace FishTank.View.Forms
{
    partial class FishTank : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FishTank));
            this.fishTankPictureBox = new System.Windows.Forms.PictureBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.labelCurrentNumberOfFish = new System.Windows.Forms.Label();
            this.labelNumberOfFishDesired = new System.Windows.Forms.Label();
            this.buttonFillTank = new System.Windows.Forms.Button();
            this.textBoxUserInput = new System.Windows.Forms.TextBox();
            this.labelNumber = new System.Windows.Forms.Label();
            this.buttonEmptyTank = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.WeightColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FishTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSortByWeight = new System.Windows.Forms.Button();
            this.buttonSortByWeightAndType = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // fishTankPictureBox
            // 
            this.fishTankPictureBox.BackColor = System.Drawing.Color.MediumBlue;
            this.fishTankPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fishTankPictureBox.BackgroundImage")));
            this.fishTankPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fishTankPictureBox.Location = new System.Drawing.Point(12, 12);
            this.fishTankPictureBox.Name = "fishTankPictureBox";
            this.fishTankPictureBox.Size = new System.Drawing.Size(700, 450);
            this.fishTankPictureBox.TabIndex = 0;
            this.fishTankPictureBox.TabStop = false;
            this.fishTankPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.fishTankCanvasPictureBox_Paint);
            // 
            // animationTimer
            // 
            this.animationTimer.Interval = 200;
            this.animationTimer.Tick += new System.EventHandler(this.animationTimer_Tick);
            // 
            // labelCurrentNumberOfFish
            // 
            this.labelCurrentNumberOfFish.AutoSize = true;
            this.labelCurrentNumberOfFish.Location = new System.Drawing.Point(38, 483);
            this.labelCurrentNumberOfFish.Name = "labelCurrentNumberOfFish";
            this.labelCurrentNumberOfFish.Size = new System.Drawing.Size(121, 13);
            this.labelCurrentNumberOfFish.TabIndex = 1;
            this.labelCurrentNumberOfFish.Text = "Current Number of Fish: ";
            // 
            // labelNumberOfFishDesired
            // 
            this.labelNumberOfFishDesired.AutoSize = true;
            this.labelNumberOfFishDesired.Location = new System.Drawing.Point(203, 483);
            this.labelNumberOfFishDesired.Name = "labelNumberOfFishDesired";
            this.labelNumberOfFishDesired.Size = new System.Drawing.Size(143, 13);
            this.labelNumberOfFishDesired.TabIndex = 2;
            this.labelNumberOfFishDesired.Text = "How many fish do you want?";
            // 
            // buttonFillTank
            // 
            this.buttonFillTank.Location = new System.Drawing.Point(458, 477);
            this.buttonFillTank.Name = "buttonFillTank";
            this.buttonFillTank.Size = new System.Drawing.Size(75, 23);
            this.buttonFillTank.TabIndex = 3;
            this.buttonFillTank.Text = "Fill Tank";
            this.buttonFillTank.UseVisualStyleBackColor = true;
            this.buttonFillTank.Click += new System.EventHandler(this.buttonFillTank_Click);
            // 
            // textBoxUserInput
            // 
            this.textBoxUserInput.Location = new System.Drawing.Point(352, 480);
            this.textBoxUserInput.Name = "textBoxUserInput";
            this.textBoxUserInput.Size = new System.Drawing.Size(100, 20);
            this.textBoxUserInput.TabIndex = 4;
            this.textBoxUserInput.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(165, 483);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(13, 13);
            this.labelNumber.TabIndex = 5;
            this.labelNumber.Text = "0";
            // 
            // buttonEmptyTank
            // 
            this.buttonEmptyTank.Location = new System.Drawing.Point(585, 477);
            this.buttonEmptyTank.Name = "buttonEmptyTank";
            this.buttonEmptyTank.Size = new System.Drawing.Size(75, 23);
            this.buttonEmptyTank.TabIndex = 6;
            this.buttonEmptyTank.Text = "Empty Tank";
            this.buttonEmptyTank.UseVisualStyleBackColor = true;
            this.buttonEmptyTank.Click += new System.EventHandler(this.buttonEmptyTank_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WeightColumn,
            this.FishTypeColumn});
            this.dataGridView1.Location = new System.Drawing.Point(719, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 449);
            this.dataGridView1.TabIndex = 7;
            // 
            // WeightColumn
            // 
            this.WeightColumn.HeaderText = "Weight";
            this.WeightColumn.Name = "WeightColumn";
            // 
            // FishTypeColumn
            // 
            this.FishTypeColumn.HeaderText = "Fish Type";
            this.FishTypeColumn.Name = "FishTypeColumn";
            // 
            // buttonSortByWeight
            // 
            this.buttonSortByWeight.Location = new System.Drawing.Point(752, 477);
            this.buttonSortByWeight.Name = "buttonSortByWeight";
            this.buttonSortByWeight.Size = new System.Drawing.Size(75, 62);
            this.buttonSortByWeight.TabIndex = 8;
            this.buttonSortByWeight.Text = "Sort by Weight";
            this.buttonSortByWeight.UseVisualStyleBackColor = true;
            this.buttonSortByWeight.Click += new System.EventHandler(this.buttonSortByWeight_Click);
            // 
            // buttonSortByWeightAndType
            // 
            this.buttonSortByWeightAndType.Location = new System.Drawing.Point(850, 477);
            this.buttonSortByWeightAndType.Name = "buttonSortByWeightAndType";
            this.buttonSortByWeightAndType.Size = new System.Drawing.Size(75, 62);
            this.buttonSortByWeightAndType.TabIndex = 9;
            this.buttonSortByWeightAndType.Text = "Sort by Weight And Type";
            this.buttonSortByWeightAndType.UseVisualStyleBackColor = true;
            this.buttonSortByWeightAndType.Click += new System.EventHandler(this.buttonSortByWeightAndType_Click);
            // 
            // FishTank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 551);
            this.Controls.Add(this.buttonSortByWeightAndType);
            this.Controls.Add(this.buttonSortByWeight);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonEmptyTank);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.textBoxUserInput);
            this.Controls.Add(this.buttonFillTank);
            this.Controls.Add(this.labelNumberOfFishDesired);
            this.Controls.Add(this.labelCurrentNumberOfFish);
            this.Controls.Add(this.fishTankPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FishTank";
            this.Text = "FishTank A4 by Li and Underwood";
            this.Load += new System.EventHandler(this.FishTank_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox fishTankPictureBox;
        private Timer animationTimer;
        private Label labelCurrentNumberOfFish;
        private Label labelNumberOfFishDesired;
        private Button buttonFillTank;
        private TextBox textBoxUserInput;
        private Label labelNumber;
        private Button buttonEmptyTank;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn WeightColumn;
        private DataGridViewTextBoxColumn FishTypeColumn;
        private Button buttonSortByWeight;
        private Button buttonSortByWeightAndType;
    }
}

