﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on the screen.
    /// </summary>
    public class TigerSharkSprite : SharkSprite
    {
        private Point[] stripeCoordinates;

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TigerSharkSprite" /> class.
        ///     Precondition: fish != null
        /// </summary>
        /// <param name="fish">The fish.</param>
        public TigerSharkSprite(Fish fish) : base(fish)
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }
            TheFish = fish;

            this.setSripeCoordinates();
        }

        #endregion

        /// <summary>
        /// Paints the fish to face left
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintLeftFacingBody(Graphics graphics)
        {
            base.PaintLeftFacingBody(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintStripe(graphics, FlipCoordinatesHorizontal(this.stripeCoordinates), offsetX, offsetY);
        }

        /// <summary>
        /// Paints a fish to face right
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintRightFacingBody(Graphics graphics)
        {
            base.PaintRightFacingBody(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintStripe(graphics, this.stripeCoordinates, offsetX, offsetY);
        }

        private void paintStripe(Graphics graphics, Point[] coordinates, int offsetX, int offsetY)
        {
            var redBrush = new SolidBrush(Color.Red);

            AdjustedCoordinates = coordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();

            graphics.FillPolygon(redBrush, AdjustedCoordinates);
        }

        private void setSripeCoordinates()
        {
            Point[] stripe =
            {
                new Point {X = 50, Y = 7},
                new Point {X = 60, Y = 0},
                new Point {X = 90, Y = 40},
                new Point {X = 70, Y = 43}
            };
            ResizeShape(stripe);
            this.stripeCoordinates = stripe;

        }
    }
}