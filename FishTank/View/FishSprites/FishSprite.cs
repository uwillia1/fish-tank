﻿using System;
using System.Drawing;
using System.Drawing.Text;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    ///     Provides a base class for different types of fish
    ///     Author: William Underwood
    ///     Version: 19 Sep 2015
    /// </summary>
    public abstract class FishSprite
    {
      
        private const double MinScale = 0.4;
        private const double MaxScale = 1.0;

        #region Properties

        /// <summary>
        /// Dictates whether a fish faces right or left
        /// </summary>
        protected int Direction { get; set; }

        /// <summary>
        /// A Fish object
        /// </summary>
        public Fish TheFish { get; set; }

        /// <summary>
        /// The new coordinates after the fish has moved/swam
        /// </summary>
        public Point[] AdjustedCoordinates { get; protected set; }

        /// <summary>
        /// Gets the scale factor.
        /// </summary>
        /// <value>
        /// The scale factor.
        /// </value>
        public double ScaleFactor
        {
            get { return this.getScaleFactor(); }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FishSprite"/> class.
        /// </summary>
        protected FishSprite()
        {
            this.TheFish = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish</param>
        /// <exception cref="ArgumentNullException">fish</exception>
        protected FishSprite(Fish fish)
            : this()
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }
            this.TheFish = fish;
        }
        #endregion

        /// <summary>
        /// Paints a fish facing right
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract void PaintRightFacingBody(Graphics graphics);

        /// <summary>
        /// Paints a fish facing left
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract void PaintLeftFacingBody(Graphics graphics);

        /// <summary>
        /// Ensures that needed objects are not null
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        protected void PaintErrorCheck(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }
            if (this.TheFish == null)
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// Gets the width of the fish.
        /// </summary>
        /// <returns>the width of the fish in interger</returns>
        public abstract int GetFishWidth();

        /// <summary>
        /// Gets the height of the fish.
        /// </summary>
        /// <returns>the height of the fish in integer</returns>
        public abstract int GetFishHeight();

        private double getScaleFactor()
        {
            var weightScale = (this.TheFish.Weight - this.TheFish.MinWeight) 
                / (this.TheFish.MaxWeight - this.TheFish.MinWeight);
            return MinScale + (MaxScale - MinScale)*weightScale;
        }
    }
}