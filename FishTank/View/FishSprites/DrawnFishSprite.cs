﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    ///     Drawn Fish Sprite is a kind of FishSprite that are drawn by setting coordinates.
    /// </summary>
    public abstract class DrawnFishSprite : FishSprite
    {
        /// <summary>
        ///     The eye number
        /// </summary>
        protected const int EyeRadius = 5;

        /// <summary>
        ///     Positions the eye on the body
        /// </summary>
        protected int EyeXCoordOffset = 95;

        /// <summary>
        ///     Positions the eye on the body
        /// </summary>
        protected int EyeYCoordOffset = 17;

        /// <summary>
        ///     The body coordinates
        /// </summary>
        public Point[] BodyCoordinates;

        #region Constructors

        /// <summary>
        ///     Creates a new instance of a DrawnFishSprite
        /// </summary>
        protected DrawnFishSprite()
        {
        }

        /// <summary>
        ///     Creates a new DrawnFishSprite
        /// </summary>
        /// <param name="fish"></param>
        protected DrawnFishSprite(Fish fish)
            : base(fish)
        {
        }

        #endregion

        /// <summary>
        ///     Other sprites will inherit and override this method.
        ///     Paints a fish.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract void Paint(Graphics graphics);

        /// <summary>
        ///     Paints a fish facing right
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract override void PaintRightFacingBody(Graphics graphics);

        /// <summary>
        ///     Paints a fish facing left
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract override void PaintLeftFacingBody(Graphics graphics);

        /// <summary>
        ///     Gets the width of the fish.
        /// </summary>
        /// <returns>
        ///     the width of the fish in interger
        /// </returns>
        public override int GetFishWidth()
        {
            var rightmostPoint = 0;
            for (var i = 0; i < this.BodyCoordinates.Length - 2; i++)
            {
                var currentPoint = this.BodyCoordinates[i];
                var x = currentPoint.X;
                rightmostPoint = this.setRightMostPoint(rightmostPoint, x);
            }
            return rightmostPoint;
        }

        /// <summary>
        ///     Returns the height of the FishSprite
        /// </summary>
        /// <returns> the fish height</returns>
        public override int GetFishHeight()
        {
            var bottommostPoint = 0;
            for (var i = 0; i < this.BodyCoordinates.Length - 2; i++)
            {
                var currentPoint = this.BodyCoordinates[i];
                var y = currentPoint.Y;
                bottommostPoint = this.setBottomMostPoint(bottommostPoint, y);
            }
            return bottommostPoint;
        }

        /// <summary>
        ///     Flips a fish's body coordinates horizontaly
        /// </summary>
        /// <returns>A set of coordinates that force a fish to face the opposite direction</returns>
        protected Point[] FlipCoordinatesHorizontal(Point[] coordinates)
        {
            Direction = Math.Abs(Direction - 1);
            var width = this.GetFishWidth();
            var flippedBodyCoordinates = new Point[coordinates.Length];
            var index = 0;
            foreach (var currentPoint in coordinates)
            {
                var xCoord = currentPoint.X;
                var yCoord = currentPoint.Y;
                xCoord = width - xCoord;
                flippedBodyCoordinates[index] = new Point {X = xCoord, Y = yCoord};
                index++;
            }
            return flippedBodyCoordinates;
        }

        /// <summary>
        ///     Resizes the sprite.
        /// </summary>
        /// Postcondition: the points are changed accourding to the scale percentage.
        protected void ResizeShape(Point[] points)
        {
            var myMatrix = new Matrix();
            myMatrix.Scale((float) ScaleFactor, (float) ScaleFactor, MatrixOrder.Append);
            myMatrix.TransformPoints(points);
        }

        /// <summary>
        ///     Draws an eye for fish facing left
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="brush"> brush</param>
        /// <param name="offsetX"> offset x</param>
        /// <param name="offsetY"> offset x</param>
        protected void DrawLeftFacingEye(Graphics graphics, SolidBrush brush, int offsetX, int offsetY)
        {
            this.drawLeftFacingEyeToScale(graphics, brush, offsetX, offsetY);
        }

        private void drawLeftFacingEyeToScale(Graphics graphics, SolidBrush brush, int offsetX, int offsetY)
        {
            this.drawRightFacingEyeToScale(graphics, brush, offsetX, offsetY);
        }

        /// <summary>
        ///     Draws an eye for fish facing right
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="brush"> brush</param>
        /// <param name="offsetX"> offset for x</param>
        /// <param name="offsetY"> offset for y</param>
        protected void DrawRightFacingEye(Graphics graphics, SolidBrush brush, int offsetX, int offsetY)
        {
            var adjustedRadius = this.adjustToScale(EyeRadius);
            var adjustedEyeXOffset = this.adjustToScale(this.EyeXCoordOffset);
            var adjustedEyeYOffset = this.adjustToScale(this.EyeYCoordOffset);

            graphics.FillEllipse(brush, (float) (offsetX + adjustedEyeXOffset), (float) (offsetY + adjustedEyeYOffset),
                (float) adjustedRadius, (float) adjustedRadius);
        }

        private void drawRightFacingEyeToScale(Graphics graphics, SolidBrush brush, int offsetX, int offsetY)
        {
            var adjustedRadius = this.adjustToScale(EyeRadius);
            var adjustedEyeXOffset = this.adjustToScale(this.EyeXCoordOffset);
            var adjustedEyeYOffset = this.adjustToScale(this.EyeYCoordOffset);

            graphics.FillEllipse(brush, (float) (offsetX + (this.GetFishWidth() - adjustedEyeXOffset)),
                (float) (offsetY + adjustedEyeYOffset), (float) adjustedRadius, (float) adjustedRadius);
        }

        private double adjustToScale(int number)
        {
            var adjustedNumber = number*ScaleFactor;
            return adjustedNumber;
        }

        private int setRightMostPoint(int rightmostPoint, int x)
        {
            if (x > rightmostPoint)
            {
                rightmostPoint = x;
            }
            return rightmostPoint;
        }

        private int setBottomMostPoint(int bottommostPoint, int y)
        {
            if (y > bottommostPoint)
            {
                bottommostPoint = y;
            }
            return bottommostPoint;
        }
    }
}
