﻿using System.Drawing;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    /// This class models Fish Sprites that takes a picture file
    /// </summary>
    public abstract class PictureFishSprite : FishSprite
    {
        /// <summary>
        /// Gets the left facing fish image.
        /// </summary>
        /// <value>
        /// The left facing fish.
        /// </value>
        public Image RightFacingFish { get; protected set; }

        /// <summary>
        /// Gets or sets the left facing fish image.
        /// </summary>
        /// <value>
        /// The left facing fish.
        /// </value>
        public Image LeftFacingFish { get; protected set; }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PictureFishSprite"/> class.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        protected PictureFishSprite()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PictureFishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish</param>
        protected PictureFishSprite(Fish fish)
        {
            TheFish = fish;
        }

        #endregion

        /// <summary>
        /// Paints a fish facing right
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract override void PaintRightFacingBody(Graphics graphics);

        /// <summary>
        /// Paints a fish facing left
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public abstract override void PaintLeftFacingBody(Graphics graphics);

        /// <summary>
        /// Gets the width of the fish.
        /// </summary>
        /// <returns>
        /// the width of the fish in interger
        /// </returns>
        public abstract override int GetFishWidth();

        /// <summary>
        /// Gets the height of the fish.
        /// </summary>
        /// <returns>
        /// the height of the fish in integer
        /// </returns>
        public abstract override int GetFishHeight();
    }
}