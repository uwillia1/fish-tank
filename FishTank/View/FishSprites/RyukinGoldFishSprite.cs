﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    /// This class models a Ryukin Gold Fish Sprite.
    /// </summary>
    public class RyukinGoldFishSprite : DrawnFishSprite
    {
        private Point[] redSpotCoordinates;

        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="RyukinGoldFishSprite" /> class.
        ///     Precondition: fish != null
        /// </summary>
        /// <param name="fish">The fish.</param>
        public RyukinGoldFishSprite(Fish fish) : base(fish)
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }
            TheFish = fish;
            this.setBodyCoordinates();
            this.setRedSpotCoordinates();
        }

        /// <summary>
        /// Creates a new Ryukin gold fish facing a random direction, left or right
        /// Precondition - direction cannot be less than zero or greater than one
        /// Postcondition - a new Ryukin gold fish was created
        /// </summary>
        /// <param name="fish"></param>
        /// <param name="direction"></param>
        public RyukinGoldFishSprite(Fish fish, int direction)
            : this(fish)
        {
            if (direction < 0 || direction > 1)
            {
                throw new ArgumentOutOfRangeException("direction");
            }
            Direction = direction;
        }

        #endregion

        /// <summary>
        /// Paints a Ryukin gold fish's body, eye, spot, and tail
        /// Precondition: Graphics object cannot be null
        /// Postcondition: Painted a Ryukin gold fish with a white body, black eye, red spot, and long white tail
        /// </summary>
        /// <param name="graphics"></param>
        public override void Paint(Graphics graphics)
        {
            PaintErrorCheck(graphics);
        }

        /// <summary>
        /// Paints the fish to face left
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintLeftFacingBody(Graphics graphics)
        {
            PaintErrorCheck(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintRyukinGoldFishBody(graphics, FlipCoordinatesHorizontal(BodyCoordinates), offsetX, offsetY);
            this.paintHat(graphics, FlipCoordinatesHorizontal(this.redSpotCoordinates), offsetX, offsetY);
            DrawLeftFacingEye(graphics, new SolidBrush(Color.Black), offsetX, offsetY);
        }

        /// <summary>
        /// Paints the fish to face right
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintRightFacingBody(Graphics graphics)
        {
            PaintErrorCheck(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintRyukinGoldFishBody(graphics, BodyCoordinates, offsetX, offsetY);
            this.paintHat(graphics, this.redSpotCoordinates, offsetX, offsetY);
            DrawRightFacingEye(graphics, new SolidBrush(Color.Black), offsetX, offsetY);
        }

        private void paintRyukinGoldFishBody(Graphics graphics, Point[] coordinates, int offsetX, int offsetY)
        {
            var brush = new SolidBrush(Color.White);

            AdjustedCoordinates = coordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();

            graphics.FillPolygon(brush, AdjustedCoordinates);
        }

        private void paintHat(Graphics graphics, Point[] spotCoordinates, int offsetX, int offsetY)
        {
            var brush = new SolidBrush(Color.Red);
            AdjustedCoordinates =
                spotCoordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();
            graphics.FillPolygon(brush, AdjustedCoordinates);
        }

        private void setBodyCoordinates()
        {
            BodyCoordinates = new[]
            {
                new Point {X = 0, Y = 0},
                new Point {X = 40, Y = 20},
                new Point {X = 60, Y = 0},
                new Point {X = 90, Y = 0},
                new Point {X = 100, Y = 15},
                new Point {X = 115, Y = 20},
                new Point {X = 115, Y = 25},
                new Point {X = 80, Y = 40},
                new Point {X = 40, Y = 30},
                new Point {X = 15, Y = 50},
                new Point {X = 15, Y = 30},
                new Point {X = 25, Y = 25},
                new Point {X = 0, Y = 20}
            };

            ResizeShape(BodyCoordinates);
        }

        private void setRedSpotCoordinates()
        {
            this.redSpotCoordinates = new[]
            {
                new Point {X = 40, Y = 20},
                new Point {X = 60, Y = 0},
                new Point {X = 90, Y = 0},
                new Point {X = 100, Y = 15}
            };

            ResizeShape(this.redSpotCoordinates);
        }
    }
}