﻿using FishTank.Utility;
using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    internal class Dolphin : Fish
    {
        public Dolphin()
        {
            MoveUpChance = .1;
            MoveDownChance = .1;
            MoveBackChance = .1;

            MinWeight = 1.5;
            MaxWeight = 20.5;

            Weight = Randomizer.GetNextRandomDouble(MinWeight, MaxWeight);
            Sprite = new DolphinSprite(this);
        }
    }
}
