﻿using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    /// <summary>
    /// A shark with a stripe
    /// </summary>
    public class TigerShark : Shark
    {
        /// <summary>
        /// Creates a new TigerShark
        /// </summary>
        public TigerShark()
        {
            Sprite = new TigerSharkSprite(this);
        }
    }
}
