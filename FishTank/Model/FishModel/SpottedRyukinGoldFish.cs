﻿using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    /// <summary>
    /// Spotted version of RyukinGoldFish.
    /// </summary>
    class SpottedRyukinGoldFish : RyukinGoldFish
    {
        /// <summary>
        /// Creates a new SpottedRyukinGoldFish.
        /// </summary>
        public SpottedRyukinGoldFish()
        {
            Sprite = new SpottedRyukinGoldFishSprite(this);
        }
    }
}
