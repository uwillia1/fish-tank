using System;
using System.Drawing;
using FishTank.Utility;
using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    /// <summary>
    ///     Holds information about a fish
    ///     Models a fish with location, direction, and movement
    /// </summary>
    public abstract class Fish
    {
        #region Data members

        /// <summary>
        /// The direction fish is heading towards
        /// </summary>
        public enum Direction
        {
            /// <summary>
            /// The left
            /// </summary>
            Left,

            /// <summary>
            /// The right
            /// </summary>
            Right
        }

        /// <summary>
        /// The move back chance in decimal
        /// </summary>
        protected double MoveBackChance;

        /// <summary>
        /// The move up chance in decimal
        /// </summary>
        protected double MoveUpChance;

        /// <summary>
        /// The move down chance in decimal
        /// </summary>
        protected double MoveDownChance;

        private Point location;

        private const int DistanceFloor = 3;
        private const int DistanceCeiling = 8;
        private readonly int moveDistance = Randomizer.GetNextRandom(DistanceFloor, DistanceCeiling + 1);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the x location of the fish.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.location.X; }
            set { this.location.X = value; }
        }

        /// <summary>
        ///     Gets or sets the y location of the fish.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location.Y = value; }
        }

        /// <summary>
        /// Used to access or change the FishSprite. 
        /// Only child classes can change the FishSprite.
        /// </summary>
        public FishSprite Sprite { get; protected set; }

        /// <summary>
        /// Gets the width of the fish sprite.
        /// </summary>
        /// <value>
        /// The width of the fish sprite.
        /// </value>
        public int Width
        {
            get { return this.Sprite.GetFishWidth(); }
        }

        /// <summary>
        /// Gets the height of the fish sprite.
        /// </summary>
        /// <value>
        /// The height of the fish sprite.
        /// </value>
        public int Height
        {
            get { return this.Sprite.GetFishHeight(); }
        }

        /// <summary>
        /// Enum class for fish heading direction
        /// 
        /// </summary>
        public Direction HeadDirection { get; set; }

        /// <summary>
        /// Gets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public double Weight { get; protected set; }

        /// <summary>
        /// The minimum weight
        /// </summary>
        public double MinWeight { get; protected set; }

        /// <summary>
        /// The maximum weight
        /// </summary>
        public double MaxWeight { get; protected set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new fish
        /// </summary>
        protected Fish()
        {
            this.Sprite = null;
            this.Weight = Randomizer.GetNextRandomDouble(this.MinWeight, this.MaxWeight);
        }

        #endregion

        /// <summary>
        ///     Draws a fish
        ///     Precondition: graphics != NULL
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish on</param>
        /// <exception cref="ArgumentNullException">"graphics cannot be null."</exception>
        public void Paint(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }
            if (this.HeadDirection == Direction.Right)
            {
                this.Sprite.PaintRightFacingBody(graphics);
            }
            else
            {
                this.Sprite.PaintLeftFacingBody(graphics);
            }
        }

        /// <summary>
        /// Moves this instance.
        /// </summary>
        public void Move()
        {
            var direction = Randomizer.GetNextRandomDouble();

            if (direction < this.MoveUpChance)
            {
                this.moveUp();
            }
            else if (direction < this.MoveUpChance + this.MoveDownChance)
            {
                this.moveDown();
            }
            else if (direction < this.MoveUpChance + this.MoveDownChance + this.MoveBackChance)
            {
                this.moveBackward();
            }
            else
            {
                this.moveForward();
            }
        }

        private void moveForward()
        {
            if (this.HeadDirection == Direction.Right)
            {
                this.moveRight();
            }
            else
            {
                this.moveLeft();
            }
        }

        private void moveBackward()
        {
            if (this.HeadDirection == Direction.Left)
            {
                this.moveRight();
            }
            else
            {
                this.moveLeft();
            }
        }

        private void moveRight()
        {
            this.X += this.moveDistance;
        }

        private void moveLeft()
        {
            this.X -= this.moveDistance;
        }

        private void moveDown()
        {
            this.Y += this.moveDistance;
        }

        private void moveUp()
        {
            this.Y -= this.moveDistance;
        }
    }
}