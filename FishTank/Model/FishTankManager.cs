﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FishTank.Model.FishModel;
using FishTank.Utility;
using System.Linq;

namespace FishTank.Model
{
    /// <summary>
    ///     Manages the collection of fish in the tank.
    /// </summary>
    public class FishTankManager
    {
        #region Data members

        //private List<Fish> schoolOfFish;
        private readonly int tankHeight;
        private readonly int tankWidth;

        #endregion

        /// <summary>
        /// A collection of Fish
        /// </summary>
        public List<Fish> SchoolOfFish { get; private set; }

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="FishTankManager" /> class from being created.
        /// </summary>
        private FishTankManager()
        {
            this.SchoolOfFish = null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTankManager" /> class.
        ///     Precondition: None
        /// </summary>
        /// <param name="height">The height of the tank.</param>
        /// <param name="width">The width of the tank.</param>
        public FishTankManager(int height, int width)
            : this()
        {
            this.tankHeight = height;
            this.tankWidth = width;
        }

        #endregion

        /// <summary>
        ///     Places 3 random fish into the tank
        ///     Precondition: None
        /// </summary>
        public void PlaceFishInTank()
        {
            this.SchoolOfFish = new List<Fish>();
            for (var i = 0; i < 2; i++)
            {
                this.SchoolOfFish.Add(FishFactory.CreateAFish());
            }
        }

        /// <summary>
        /// Allows a desired number of fish to be specified.
        /// The desired number of fish will appear in the tank.
        /// Precondition - numberofFish cannot be less than or equal to zero
        /// Postcondition - The specified number of fish appeared in the tank.
        ///     Allows a desired number of fish to be specified.
        ///     The desired number of fish will appear in the tank.
        ///     Precondition - numberofFish cannot be less than or equal to zero
        ///     Postcondition - The specified number of fish appeared in the tank.
        /// </summary>
        /// <param name="numberOfFish"> number of fish to be placed in tank</param>
        public void PlaceFishInTank(int numberOfFish)
        {
            this.SchoolOfFish = new List<Fish>(numberOfFish);
            for (var i = 0; i < numberOfFish; i++)
            {
                var fish = FishFactory.CreateAFish();
                fish.X = Randomizer.GetNextRandom(this.tankWidth - fish.Width);
                fish.Y = Randomizer.GetNextRandom(this.tankHeight - fish.Height);
                fish.HeadDirection = this.selectRandomDirection();
                this.SchoolOfFish.Add(fish);
            }
        }

        /// <summary>
        ///     Moves the fish around and the calls the Fish::Paint method to draw the fish.
        ///     Precondition: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw on</param>
        public void Update(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }
            foreach (var fish in this.SchoolOfFish)
            {
                this.turnAround(fish);
                fish.Move();
                this.turnAround(fish);
                fish.Paint(graphics);
            }
        }

        private void turnAround(Fish myFish)
        {
            if (myFish.X <= 0)
            {
                myFish.HeadDirection = Fish.Direction.Right;
                myFish.X = 0;
            }
            else if (myFish.X >= this.tankWidth - myFish.Sprite.GetFishWidth())
            {
                myFish.HeadDirection = Fish.Direction.Left;
                myFish.X = this.tankWidth - myFish.Sprite.GetFishWidth();
            }

            if (myFish.Y <= 0)
            {
                myFish.Y = 0;
            }
            else if (myFish.Y >= this.tankHeight - myFish.Sprite.GetFishHeight())
            {
                myFish.Y = this.tankHeight - myFish.Sprite.GetFishHeight();
            }
        }

        private Fish.Direction selectRandomDirection()
        {
            var values = Enum.GetValues(typeof(Fish.Direction));
            var randomDirection = (Fish.Direction)values.GetValue(Randomizer.GetNextRandom(values.Length));
            return randomDirection;
        }

        /// <summary>
        /// Returns a collection of Fish that have been sorted in ascending order by their weight.
        /// </summary>
        /// <returns>A collection of Fish objects sorted in ascending order</returns>
        public List<Fish> SortByWeight()
        {
            var result = 
                this.SchoolOfFish.OrderBy(fish => fish.Weight).ToList();
            return result;
        }

        /// <summary>
        /// Returns a collection of Fish that have been sorted in ascending order by their type then their weight.
        /// </summary>
        /// <returns>A collection of Fish objects sorted in ascending order</returns>
        public List<Fish> SortByTypeThenWeight()
        {
            var result = 
                this.SchoolOfFish.OrderBy(fish => fish.GetType().Name).ThenBy(fish => fish.Weight).ToList();
            return result;
        } 
    }
}
